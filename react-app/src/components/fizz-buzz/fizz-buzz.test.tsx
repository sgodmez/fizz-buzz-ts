/**
 * @jest-environment jsdom
 */
import { render, screen } from "@testing-library/react";
import { FizzBuzz } from "./fizz-buzz";
import React from "react";
import { setupServer } from "msw/node";
import { rest } from "msw";

const server = setupServer(
  rest.get("/api/fizz-buzz", (_, res, ctx) => {
    return res(
      ctx.json([
        "1",
        "2",
        "Fizz",
        "4",
        "Buzz",
        "Fizz",
        "7",
        "8",
        "Fizz",
        "Buzz",
        "11",
        "Fizz",
        "13",
        "14",
        "FizzBuzz",
      ])
    );
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe("FizzBuzz component test", () => {
  test("Test header", () => {
    render(<FizzBuzz />);
    const heading = screen.getByRole("heading", { level: 2 });
    expect(heading).toBeInTheDocument();
    expect(heading.textContent).toStrictEqual("Fizz Buzz List");
  });
});
