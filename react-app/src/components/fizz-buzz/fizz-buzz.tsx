import type { FC } from "react";
import React, { useEffect, useState } from "react";
import { getFizzBuzz } from "../../services/fizz-buzz.service";

export const FizzBuzz: FC = () => {
  const [data, setData] = useState<string[]>([]);

  useEffect(() => {
    getFizzBuzz().then(setData);
  }, []);

  return (
    <>
      <h2>Fizz Buzz List</h2>
      <ul className="list-todo">
        {data.map((value, i) => (
          <li key={`${i}`} className="fizz-buzz-item">
            {value}
          </li>
        ))}
      </ul>
    </>
  );
};
