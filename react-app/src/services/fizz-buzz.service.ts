export const getFizzBuzz = async (): Promise<string[]> => {
  const res = await fetch(`http://localhost:5001/api/fizz-buzz`);
  if (res.ok) return res.json();
  return [];
};
