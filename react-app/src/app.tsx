import React from "react";
import { FizzBuzz } from "./components/fizz-buzz/fizz-buzz";

export const App = () => {
  return (
    <>
      <h1>Welcome</h1>
      <FizzBuzz />
    </>
  );
};
