# Fizz Buzz TS

Simple fizz buzz in TypeScript language.

Rules :
  - display the numbers from 1 to 100 
  - for multiples of three print “Fizz” instead of the number 
  - for the multiples of five print “Buzz” 
  - for numbers which are multiples of both three and five print “FizzBuzz”

## Project structure

The project structure is the following :

`├── api                 Contains the api application. `  
`└── react-app            Contains the frontend application.`   

## Pre-requisites

- [NodeJS](https://nodejs.org/en/) > 14 is installed

## Built With 🔧

- [TypeScript](https://www.typescriptlang.org/) - Main project language. Open-source language which builds on JavaScript
- [NodeJS](https://nodejs.org/) - JavaScript runtime environment
- [Express](https://expressjs.com/fr/) - Back end web application framework for Node.js
- [Jest](https://jestjs.io/) - Testing framework
- [Npm](https://www.npmjs.com/) - Dependency Management
- [Pino](https://www.npmjs.com/package/pino) - Very low overhead Node.js logger

## Installation

Under react-app and api directories :

- run `npm install`
- run `npm run start`

Enjoy at [http://localhost:3000](http://localhost:3000)