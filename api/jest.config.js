module.exports = {
  preset: 'ts-jest',
  coverageReporters: ['json', 'lcov', 'text', 'clover', 'cobertura'],
  moduleNameMapper: {
    '@domains/(.*)': ['<rootDir>/src/domains/$1'],
    '@core/(.*)': ['<rootDir>/src/core/$1'],
  },
}
