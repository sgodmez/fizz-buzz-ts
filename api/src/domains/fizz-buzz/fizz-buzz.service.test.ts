import { getFizzBuzz } from '@domains/fizz-buzz/fizz-buzz.service'

const FIZZ = 'Fizz'
const BUZZ = 'Buzz'

describe('Fizz Buzz test', () => {
  let fizzBuzzArray: string[]
  beforeAll(async () => {
    fizzBuzzArray = getFizzBuzz()
  })

  describe('Skeleton Test', () => {
    it('returns an Array', () => {
      expect(typeof fizzBuzzArray === 'object').toBe(true)
      expect(Array.isArray(fizzBuzzArray)).toBe(true)
    })

    it('returns an Array of 100 elements', () => {
      expect(fizzBuzzArray.length).toBe(100)
    })
  })

  describe('Number Test', () => {
    it('1 is a not a multiple of 3 or 5 => display it', () => {
      expect(fizzBuzzArray[0]).toBe('1')
    })
    it('4 is a not a multiple of 3 or 5 => display it', () => {
      expect(fizzBuzzArray[3]).toBe('4')
    })
  })

  describe('Fizz Test', () => {
    it('3 is a multiple of 3 => replaced by Fizz', () => {
      expect(fizzBuzzArray[2]).toBe(FIZZ)
    })
    it('99 is a multiple of 3 => replaced by Buzz', () => {
      expect(fizzBuzzArray[98]).toBe(FIZZ)
    })
  })

  describe('Buzz Test', () => {
    it('5 is a multiple of 5 => replaced by Buzz', () => {
      expect(fizzBuzzArray[4]).toBe(BUZZ)
    })
    it('100 is a multiple of 5 => replaced by Buzz', () => {
      expect(fizzBuzzArray[99]).toBe(BUZZ)
    })
  })

  describe('Fizz Buzz Test', () => {
    it('15 is a multiple of 5 and 3 => replaced by FizzBuzz', () => {
      expect(fizzBuzzArray[14]).toBe(FIZZ + BUZZ)
    })
  })
})
