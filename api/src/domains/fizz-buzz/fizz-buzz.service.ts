const FIZZ = 'Fizz'
const BUZZ = 'Buzz'
const ARRAY_LENGTH = 100

const getFizzBuzz = (): string[] => {
  return Array.from(Array(ARRAY_LENGTH), (_, i) => {
    const val = i + 1
    let ret = ''
    if (val % 3 === 0) ret = ret.concat(FIZZ)
    if (val % 5 === 0) ret = ret.concat(BUZZ)
    if (ret === '') ret = val.toString(10)
    return ret
  })
}

export { getFizzBuzz }
