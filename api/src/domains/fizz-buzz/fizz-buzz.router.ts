import express, { Request, Response } from 'express'
import { getFizzBuzz } from './fizz-buzz.service'

/**
 * Router Definition
 */
const fizzBuzzRouter = express.Router()

// GET fizz buzz
fizzBuzzRouter.get('/api/fizz-buzz', async (_: Request, res: Response) => {
  return res.json(getFizzBuzz())
})

export default fizzBuzzRouter
