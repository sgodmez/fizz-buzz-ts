/**
 * Required External Modules
 */
import { Logger } from "@core/logger";
import express from "express";
import cors from "cors";
import fizzBuzzRouter from "@domains/fizz-buzz/fizz-buzz.router";

/**
 * App Variables
 */
const PORT: string | number = process.env.PORT || 5001;
const app = express();

/**
 *  App Configuration
 */
app.use(express.json());
// to modify for production usage
app.use(cors());
app.use(fizzBuzzRouter);
/**
 * Server Activation
 */
app.listen(PORT, () =>
  Logger.debug(`Server running on http://localhost:${PORT}/api/fizz-buzz`)
);
