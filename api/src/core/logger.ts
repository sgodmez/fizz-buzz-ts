import pino from 'pino'

export const Logger = pino({ level: 'debug', name: 'fizz-buzz-api' })
